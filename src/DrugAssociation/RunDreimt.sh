#!/bin/bash
# -- Script to generate all files required to process generate the back-end
# -- gmt files and annotation file must have same date
# -- Conda environment activation
currDir="$PWD"
source activate DREIMT

Npermutations=1000 # Default: 1000
Ncores=30 # Cores to parallelize fgsea
date="20200914" # Date as indicated in the input files: gmt and annotation

# -- gmt files
# -- gmt files must be correctly curated, no duplicated genes, c7 gensets must ordered
gmtSignature="../../data/Dreimt_Signatures_""$date""_clean.gmt"
gmtGeneSet="../../data/Dreimt_Genesets_""$date""_clean.gmt"

# -- Requiered inputs for drug prediction and notation
# -- relative paths
outFolder="../../Output/v""$date"
annotation="../../data/sig_id_table_LINCS_short.tsv"
drugProfile="../../data/D1_short_t_matrix_12434_4690_LINCS.rds"
annotationfile="../../data/Dreimt_curation_Base_Datos_""$date"".csv"
drug_target="../../data/drug_target_genes.tsv"
universe="../../data/D1geneUniverse.rds"

# -- create output folder
mkdir -p "$outFolder"

######################################################################################################################################################
######################################################################################################################################################

# -- DREIMT
# -- Script to generate the DB from signature
Rscript Dreimt_UPDN_tau.R "$gmtSignature" "$drugProfile" "$annotation" "$outFolder" "$Npermutations" "$Ncores" "$annotationfile"

# -- Script to generate the  DB from geneset
Rscript Dreimt_UP_tau.R "$gmtGeneSet" "$drugProfile" "$annotation" "$outFolder" "$Npermutations" "$Ncores" "$annotationfile"
#######################################################################################################################################################
# -- The data must be ordered in specific folders and formats
# -- this parte generates the folder tree as exècted
# -- date from bash to save files
bash_date=`date +"%Y%m%d"`

# -- Organize output folders
cd "$outFolder"

# -- Create more folders
mkdir Database Inputs Intermediate Precalculated
mkdir -p Precalculated/signatures/1
mkdir -p Precalculated/signatures/2

#
# -- Move to Database
cp "Dreimt_DB_SIG_Annotated_""$bash_date"".rds" Database/Dreimt_DB_SIG_Annotated.rds
cp "Dreimt_DB_GS_Annotated_""$bash_date"".rds" Database/Dreimt_DB_GS_Annotated.rds

# -- Move to Inputs
cp "$drugProfile" Inputs/D1_short_t_matrix_12434_4690_LINCS.rds
cp "$universe" Inputs/D1geneUniverse.rds
cp "$gmtGeneSet" Inputs/Dreimt_Genesets_clean.gmt
cp "$gmtSignature" Inputs/Dreimt_Signatures_clean.gmt
cp "$annotation" Inputs/sig_id_table_LINCS_short.tsv
cp "$drug_target" Inputs/drug_target_genes.tsv
cat "$annotationfile" | sed 's/,/\t/g' > Inputs/Dreimt_curation_DB.tsv

# -- Move to Intermediate
cp "DBprecalculated.Coordinated.ES.GS_""$bash_date"".rds" Intermediate/DBprecalculated.Coordinated.ES.GS.rds
cp "DBprecalculated.Coordinated.ES.SIG_""$bash_date"".rds" Intermediate/DBprecalculated.Coordinated.ES.SIG.rds
cp "Normalized_ES_GS_""$bash_date"".rds"  Intermediate/Normalized_ES_GS.rds
cp "Normalized_ES_SIG_""$bash_date"".rds"  Intermediate/Normalized_ES_SIG.rds


# -- Fill Precalculated folder with examples
############################################################################################################################################
# -- gmts for example data
# folder 1: Macrophages M1 vs M2 example
# folder 2: Th1 vs Th2 example
gmt1="../../data/COATES_MACROPHAGE_M1_VS_M2.gmt"
gmt2="../../data/GSE11924_TH1_VS_TH2_CD4_TCELL.gmt"

# example 1 metafiles
echo "description=\"Differentially expressed genes in type 1 macrophage vs type 2 macrophage\"
title=\"COATES MACROPHAGE M1 VS M2\"
reference=\"Coates et al. 2008\"
url=\"https://www.ncbi.nlm.nih.gov/pubmed/18199539\"
caseType=\"Macrophages M1\"
referenceType=\"Macrophages M2\"" > "$outFolder"/Precalculated/signatures/1/metadata

# example 2 metafiles
echo "description=\"Differentially expressed genes in type 1 T-helper vs type 2 T-helper\"
title=\"GSE11924 TH1 VS TH2 CD4 TCELL\"
reference=\"Nurieva et al. 2008\"
url=\"https://www.ncbi.nlm.nih.gov/pubmed/18599325\"
caseType=\"T-helper type 1\"
referenceType=\"T-helper type 2\"" > "$outFolder"/Precalculated/signatures/2/metadata


# -- Test hyperfeometric for example
Rscript "$currDir"/../SignatureComparison/Hypergeom_Sig.R "$gmt1" "$universe" "$gmtGeneSet" F "$outFolder"/Precalculated/signatures/1/results-jaccard.tsv
Rscript "$currDir"/../SignatureComparison/Hypergeom_Sig.R "$gmt2" "$universe" "$gmtGeneSet" F "$outFolder"/Precalculated/signatures/2/results-jaccard.tsv


echo "onlyUniverseGenes=F" > "$outFolder"/Precalculated/signatures/1/results-jaccard-params
echo "onlyUniverseGenes=F" > "$outFolder"/Precalculated/signatures/2/results-jaccard-params

echo "gseaPermutations=1000" > "$outFolder"/Precalculated/signatures/1/results-cmap-params
echo "gseaPermutations=1000" > "$outFolder"/Precalculated/signatures/2/results-cmap-params

cp "$gmt1" "$outFolder"/Precalculated/signatures/1/signature.gmt
cp "$gmt2" "$outFolder"/Precalculated/signatures/2/signature.gmt

# -- Query signature ejemplo

Rscript "$currDir"/Dreimt_UPDN_tau_UserQuery.R "$gmt1" "$drugProfile" "$annotation" "$Npermutations" "$Ncores" "$outFolder"/Intermediate/DBprecalculated.Coordinated.ES.SIG.rds "$outFolder"/Intermediate/Normalized_ES_SIG.rds "$outFolder"/Precalculated/signatures/1/results-cmap

Rscript "$currDir"/Dreimt_UPDN_tau_UserQuery.R "$gmt2" "$drugProfile" "$annotation" "$Npermutations" "$Ncores" "$outFolder"/Intermediate/DBprecalculated.Coordinated.ES.SIG.rds "$outFolder"/Intermediate/Normalized_ES_SIG.rds "$outFolder"/Precalculated/signatures/2/results-cmap

#####
cd "$outFolder"
rm *rds

# zip everything
zip -r dreimt_v"$date".zip *

# remove folder
rm -r "$outFolder"/Database "$outFolder"/Inputs "$outFolder"/Intermediate "$outFolder"/Precalculated
