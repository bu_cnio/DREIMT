#!/bin/bash

# -- Script to generate specific Jaccards

# -- Output folder
out="/local/ktroule/Dreimt/Dreimt.Tau/Output/v20200108"

# -- No tocar
gmt1="/local/ktroule/Dreimt/Dreimt.Tau/Input/examples/COATES_MACROPHAGE_M1_VS_M2.gmt"
gmt2="/local/ktroule/Dreimt/Dreimt.Tau/Input/examples/GSE11924_TH1_VS_TH2_CD4_TCELL.gmt"

universe="/local/ktroule/Dreimt/Dreimt.Tau/Input/D1geneUniverse.rds"

# -- Modificar, poner ultimo gmt
gmt_GS="/local/ktroule/Dreimt/Dreimt.Tau/Signatures/Dreimt_Genesets_20191121_clean.gmt"

Rscript Hypergeom_GS.R "$gmt1" "$universe" "$gmt_GS" F "$out"/Precalculated/signatures/1/results-jaccard.tsv
Rscript Hypergeom_GS.R "$gmt2" "$universe" "$gmt_GS" F "$out"/Precalculated/signatures/2/results-jaccard.tsv

# -- Metadata jaccard
echo "onlyUniverseGenes=F" > "$out"/Precalculated/signatures/1/results-jaccard
echo "onlyUniverseGenes=F" > "$out"/Precalculated/signatures/2/results-jaccard

# -- Metadata
echo "Differentially expressed genes in type 1 macrophage vs type 2 macrophage" > "$out"/Precalculated/signatures/1/metadata
echo "Differentially expressed genes in type 1 T-helper vs type 2 T-helper" > "$out"/Precalculated/signatures/2/metadata

# -- Gmt used
cp "$gmt1" "$out"/Precalculated/signatures/1/signature.gmt
cp "$gmt2" "$out"/Precalculated/signatures/2/signature.gmt

# "gseaPermutations=1000"
# "gseaPermutations=1000"
