# Signature comparison

Scripts to compare query signatures to those already present in de database. The script perform an Hypergeometric test and a jaccard test between query and those presente in the database.

### Code example

To run the script the following inputs are needed:

1) gene set/signature gmt input file
2) rds with genes in drug profile
3) gmt with signaures in database
4) true or false, indicating if only universe genes should be considered or not
5) the name of the output file

```
Rscript Hypergeom_GS.R /path/Single_GS_test.gmt /path/D1geneUniverse.rds /path/Dreimt_GeneSet_20190207.gmt <use_only_universe_genes> /path/output.tsv
```
```
Rscript Hypergeom_Sig.R /path/Single_SIG_test.gmt /path/D1geneUniverse.rds /path/Dreimt_GeneSet_20190207.gmt <use_only_universe_genes> /path/output.tsv
```
