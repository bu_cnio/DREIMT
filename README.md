# Dreimt
#### Drug repositioning for immune transcriptome

This DREIMT's repository.

<img src="img/logo.png" width="30%">

Drug REpositioning for IMmune Transcriptome

## Introduction
DREIMT is a bioinformatics tool for hypothesis generation and prioritization of drugs capable of **modulating immune cells** activity. DREIMT aims to **repurpose** drugs capable of modulating immune cells from **transcriptomics** data.
DREIMT integrates **4,690 drug profiles** from The Library of Network-Based Cellular Signatures (LINCS) L1000 data set and **2,700 manually curated immune gene expression signatures** from multiple sources.

Each folder contains its own readme file with information to execute all scripts.
Docker image can be found [here](https://hub.docker.com/r/singgroup/r-dreimt-scripts).


### Data
All required data can within the **data** folder.

### Scripts
To run the scripts all required dependencies must be installed, these are available as a conda yml file within the **env** folder.
Scripts are found within the **src** folder: 
- **DrugPrediction**: scripts to generate the DREIMT's database and scripts to performed single signature/gene set queries.  
- **SignatureComparison**: scripts to perform comparison between a given signature/gene set query and DREIMT's signatures.


##### Generate database
To generate DREIMT's database the script **RunDreimt.sh**, importantly before its execution make sure to define correctly input variables (within the script). If nothing is modified, the script is set up to generate the database making use of the files available at the **data** folder.  

##### Scripts
Brief description of the scripts.

|Script|Use|
|-----|-----|
|**Dreimt_UPDN_tau.R** | generates the full database and files used in single query script. Input signature composed by a pair of gene sets one Up and other down, betweem 15 and 200 genes DEG genes. |
|**Dreimt_UPDN_tau_UserQuery.R** | predicts drugs for a single query, compares against the full database to generate a tau score. Input signature must be compose by a pair of gene sets one Up other Down, between 15 and 200 DEG genes. |
|**Dreimt_UP_tau.R** | generates the full database and files used in single query for gene set. Input is a gene set, either up or down. |
|**Dreimt_UP_tau_UserQuery.R** | predicts drugs for a single query of gene set compares agains the full database to generate a tau score. Input is a gene set in the range of 15 to 200 DEG genes. |
|**RunDreimt.sh** | executes all scripts and examples to generate a zip folder to deploy DREIMT web tool. |

###### Code example for database generation
To run the script the following inputs are needed:  
1) gmt file containing signatures.
2) rds file containing drug profiles.
3) tsv file with drug annotation.
4) output folder.
5) number of permutations in GSEA.
6) number of cores for parallelization.
7) csv with annotation.


Execution to generate signature database. 
```
Rscript Dreimt_UPDN_tau.R Dreimt_Signature.gmt D1_short_t_matrix_12434_4690_LINCS.rds sig_id_table_LINCS_short.tsv Output 1000 25 annotation.csv
```
Execution to generate gene set database. 
```
Rscript Dreimt_UP_tau.R Dreimt_GeneSet.gmt D1_short_t_matrix_12434_4690_LINCS.rds sig_id_table_LINCS_short.tsv Output 1000 25 annotation.csv
```
##### Execute single queries

###### Code example for query  
To run the script the following inputs are needed:  
1) gmt file containing signatures.
2) rds file containing drug profiles.
3) tsv file with drug annotation.
4) number of permutations in GSEA.
5) number of cores for parallelization.
6) coordinated ES matrix.
7) normalized ES.
8) path with output + prefix. 


Execution if input is a signature.
```
Rscript Dreimt_UPDN_tau_UserQuery.R /path/Dreimt_Signature.gmt /path/D1_short_t_matrix_12434_4690_LINCS.rds /path/sig_id_table_LINCS_short.tsv 1000 25 path/coordESsignature.rds path/normalizedESsignature.rds path/prefix_file
```
Execution if input is a gene set.
```
Rscript Dreimt_UP_tau_UserQuery.R /path/Dreimt_GeneSet.gmt /path/D1_short_t_matrix_12434_4690_LINCS.rds /path/sig_id_table_LINCS_short.tsv 1000 25 path/coordESgeneset.rds path/normalizedESgeneset.rds path/prefix_file
```
##### Hypergeometric test
Code to perform hypergometric tests.

###### Code example
Inputs:
1) gene set/signature gmt input file.
2) rds with genes in drug profile.
3) gmt with signaures in database.
4) true or false, indicating if only genes in drug profiles should be considered or not.
5) the name of the output file.

Execution if input is a gene set.
```
Rscript Hypergeom_GS.R Single_GS.gmt D1geneUniverse.rds Dreimt_Database.gmt <use_only_universe_genes> output.tsv
```
Execution if input is a signature.
```
Rscript Hypergeom_Sig.R Single_SIG.gmt D1geneUniverse.rds Dreimt_Database.gmt <use_only_universe_genes> output.tsv
```
